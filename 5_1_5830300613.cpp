//Piyatida Sakorn 5830300613 Sec.800
#include <stdio.h>
#include <iostream>
#include <istream>
#include <string.h>
#include <string>
#include <typeinfo>
using namespace std;
struct record
{   int value=NULL;
    char oper=NULL;
    struct record *next,*prev;
};

struct record *push(struct record *head,char data)
{
	struct record *node;
	if(head==NULL)
	{
		head = new struct record;
		if(head==NULL)
		{
			cout << "Out of space!!!" << endl;
			return head;
		}
		node = new struct record;
		if(data=='+' || data=='-' || data=='*' || data=='/' || data==' ')
        {
            node->oper = data;
        }
        else
        {
            if(int(data)==48)//0
                node->value = 0;
            else if(int(data)==49)//1
                node->value = 1;
            else if(int(data)==50)//2
                node->value = 2;
            else if(int(data)==51)//3
                node->value = 3;
            else if(int(data)==52)//4
                node->value = 4;
            else if(int(data)==53)//5
                node->value = 5;
            else if(int(data)==54)//6
                node->value = 6;
            else if(int(data)==55)//7
                node->value = 7;
            else if(int(data)==56)//8
                node->value = 8;
            else if(int(data)==57)//9
                node->value = 9;
        }
		node->next = NULL;
		node->prev = head;
		head->next = node;
		head->prev = NULL;
	}
	else
	{
		node = new struct record;
		if(node==NULL)
		{
			cout << "Out of space!!!" << endl;
			return head;
		}
		if(data=='+' || data=='-' || data=='*' || data=='/' || data==' ')
        {
            node->oper = data;
        }
        else
        {
            if(int(data)==48)//0
                node->value = 0;
            else if(int(data)==49)//1
                node->value = 1;
            else if(int(data)==50)//2
                node->value = 2;
            else if(int(data)==51)//3
                node->value = 3;
            else if(int(data)==52)//4
                node->value = 4;
            else if(int(data)==53)//5
                node->value = 5;
            else if(int(data)==54)//6
                node->value = 6;
            else if(int(data)==55)//7
                node->value = 7;
            else if(int(data)==56)//8
                node->value = 8;
            else if(int(data)==57)//9
                node->value = 9;
        }
		node->next = head->next;
		node->prev = head;
		node->next->prev = node;
		head->next = node;
	}
	return head;
}

struct record *pop(struct record *head)
{
	struct record *tmp,*node;
	if(head->next==NULL)
	{
		cout << "Error, Stack Empty!!!" << endl;
	}
	else
	{
		tmp = head->next;
		head->next = head->next->next;
		if(head->next!=NULL)
        {
            head->next->prev = head;
        }
        else
        {
            head=NULL;
        }
		delete(tmp);
	}
	return head;
}

void print(struct record *head)
{
    struct record *p=head,*headp=NULL;
    while(p->next!=NULL)
    {
        p=p->next;
    }
    cout << "output : ";
    while(p->prev!=NULL)
    {
        if(p->value!=0)
        {
            if(p->prev->value !=0)
                cout << p->value ;
            else
                cout << p->value << " ";
        }
        else
        {
            if(p->oper!=' ')
            {
                if(headp==NULL)
                {
                    headp = push(headp,p->oper);
                }
                else
                {
                    if(p->oper=='*' || p->oper=='/')
                    {
                        if(headp->next->oper=='+' || headp->next->oper=='-')
                            headp = push(headp,p->oper);
                        else if(headp->next->oper=='*' || headp->next->oper=='/')
                        {
                            cout << headp->next->oper << " ";
                            headp = pop(headp);
                            headp = push(headp,p->oper);
                        }
                    }
                    else if(p->oper=='+' || p->oper=='-')
                    {
                        cout << headp->next->oper << " ";
                        headp = pop(headp);
                        headp = push(headp,p->oper);
                    }
                }
            }
        }
        p=p->prev;
    }
    while(headp!=NULL)
    {
        cout << headp->next->oper << " ";
        headp = pop(headp);
    }
}

int main()
{
    struct record node,*head=NULL;
    string line;
    cout << "input : ";
    getline(cin,line);
    for(int i=0 ; i< line.size() ;i++)
    {
        head=push(head,line[i]);
        //cout << "value" << head->next->value << endl;
    }
    print(head);
}
